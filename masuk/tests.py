from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model, get_user

class MainTestCase(TestCase):
    def test_url_masuk_200(self):
        response = self.client.get(reverse('masuk:masuk'))
        
        self.assertEqual(response.status_code, 200)
        
    def test_masuk_form_valid(self):
        self.client.post(reverse('daftar:daftar'), {'username' : 'test_user', 'password1' : 'test_password'})
        response = self.client.post(reverse('masuk:masuk'), {'username' : 'test_user', 'password' : 'test_password'}, follow = True)
        user = get_user(self.client)
        
        self.assertTrue(user.is_authenticated)
        self.assertRedirects(response, reverse('main:home'))
        
    def test_masuk_form_invalid_1(self):
        User = get_user_model()
        User.objects.create_user(username='test_user', password='test_password')
        response = self.client.post(reverse('masuk:masuk'), {'username' : 'test_user_lain', 'password' : 'test_password'})
        user = get_user(self.client)
        
        self.assertFalse(user.is_authenticated)
        self.assertFalse(User.objects.filter(username='test_user_lain').exists())
        
    def test_masuk_form_invalid_2(self):
        User = get_user_model()
        response = self.client.post(reverse('masuk:masuk'), {'username' : 'test user spasi', 'password' : 'test_password'})
        user = get_user(self.client)
        
        self.assertFalse(user.is_authenticated)
        self.assertFalse(User.objects.filter(username='test user spasi').exists())