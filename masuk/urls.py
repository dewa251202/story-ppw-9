from django.urls import path
from . import views

app_name = 'masuk'

urlpatterns = [
    path('', views.masuk, name='masuk')
]