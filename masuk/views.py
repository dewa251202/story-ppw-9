from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, login
from .forms import MasukForm

def masuk(request):
    context = {}
    if request.method == 'GET':
        empty_form = MasukForm()
        context['form'] = empty_form
    elif request.method == 'POST':
        filled_form = MasukForm(data=request.POST)
        if filled_form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            
            login_user = authenticate(request, username=username, password=password)
            
            if login_user is not None:
                login(request, login_user)
                if request.GET.get('next', False):
                    request.GET['next'].replace('%23', '#')
                    return HttpResponseRedirect(request.GET['next'])
                else:
                    return HttpResponseRedirect(reverse('main:home'))
            else:
                print("Akun tidak dikenal")
                context['form'] = filled_form
        else:
            context['form'] = filled_form
    return render(request, 'masuk/masuk.html', context)