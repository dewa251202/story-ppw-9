from django.contrib.auth import get_user_model
from django.contrib.auth.forms import AuthenticationForm

class MasukForm(AuthenticationForm):
    class Meta:
        model = get_user_model()
        fields = ['username', 'password']
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].label = "Nama Pengguna"
        self.fields['password'].label = "Kata Sandi"