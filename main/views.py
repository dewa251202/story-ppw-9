from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from .models import LatarBelakang

def home(request):
    context = {}

    user = request.user
    if user.is_authenticated:
        context['user_color'] = LatarBelakang.objects.get(user=user).warna
    else:
        context['user_color'] = "#FFF500";
    
    return render(request, 'main/home.html', context)

@login_required
def ganti_warna(request):
    if request.GET.get("new_color", False):
        changed_background = LatarBelakang.objects.get(user=request.user)
        changed_background.warna = request.GET["new_color"]
        changed_background.save()
    return HttpResponseRedirect(reverse('main:home'))
    
def keluar(request):
    logout(request)
    return HttpResponseRedirect(reverse('main:home'))