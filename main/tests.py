from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver
from django.contrib.auth import get_user

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def setUp(self):
        self.client.post(reverse('daftar:daftar'), {'username' : 'test_user', 'password1' : 'test_password'})
        
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)
        
    def test_url_ganti_warna(self):
        response = self.client.get(reverse('main:ganti_warna'), {'new_color' : '#FFFFFF'}, follow=True)
        
        self.assertRedirects(response, reverse('main:home'))
        
    def test_keluar(self):
        response = self.client.get(reverse('main:keluar'), follow=True)
        user = get_user(self.client)

        self.assertFalse(user.is_authenticated)     
        self.assertRedirects(response, reverse('main:home'))
        
class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
