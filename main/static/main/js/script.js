$(document).ready(function(){
    var colorInput = $("#color-input");
    var colorValue = colorInput.val();
    colorInput.on("input", function(){
        colorValue = colorInput.val();
        $("body").css("background-color", colorValue);
    });
    colorInput.on("change", function(){
        $.ajax({
            url: "/ganti_warna",
            data: {new_color : colorValue}
        });
    });
});