from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('ganti_warna', views.ganti_warna, name='ganti_warna'),
    path('keluar', views.keluar, name='keluar'),
]
