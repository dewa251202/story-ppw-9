from django.db import models
from django.contrib.auth.models import User

class LatarBelakang(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    warna = models.CharField(max_length=7, default="#FFF500")
    