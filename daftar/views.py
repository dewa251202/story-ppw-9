from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.models import User
from main.models import LatarBelakang
from django.contrib.auth import authenticate, login
from .forms import DaftarForm

def daftar(request):
    context = {}
    if request.method == 'GET':
        empty_form = DaftarForm()
        context['form'] = empty_form
    elif request.method == 'POST':
        filled_form = DaftarForm(request.POST)
        if filled_form.is_valid():
            username = request.POST['username']
            password = request.POST['password1']
            
            new_user = User.objects.create_user(username=username, password=password)
            LatarBelakang.objects.create(user=new_user)
            login_user = authenticate(request, username=username, password=password)
            login(request, login_user)
            
            return HttpResponseRedirect(reverse('main:home'))
        else:
            context['form'] = filled_form
    return render(request, 'daftar/daftar.html', context)