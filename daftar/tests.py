from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model, get_user

class MainTestCase(TestCase):
    def test_url_daftar_200(self):
        response = self.client.get(reverse('daftar:daftar'))
        
        self.assertEqual(response.status_code, 200)
    
    def test_daftar(self):
        User = get_user_model()
        new_user = User.objects.create_user(username='test_user', password='test_password')
        
        self.assertEqual(User.objects.count(), 1)
        self.assertTrue(new_user.is_active)
        
    def test_daftar_form_valid(self):
        User = get_user_model()
        response = self.client.post(reverse('daftar:daftar'), {'username' : 'test_user', 'password1' : 'test_password'}, follow = True)
        user = get_user(self.client)
        
        self.assertTrue(user.is_authenticated)
        self.assertEqual(User.objects.count(), 1)
        self.assertRedirects(response, reverse('main:home'))
        
    def test_daftar_form_invalid(self):
        User = get_user_model()
        response = self.client.post(reverse('daftar:daftar'), {'username' : 'test user spasi', 'password1' : ''})
        user = get_user(self.client)
        
        self.assertFalse(user.is_authenticated)
        self.assertFalse(User.objects.filter(username='test user spasi').exists())